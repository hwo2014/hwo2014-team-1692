package groundeffect.physics;

import java.io.IOException;

import groundeffect.api.messages.TestCaseWithJsonData;
import groundeffect.api.model.TrackData;
import groundeffect.api.tools.JsonLoader;

import org.junit.Test;

public class TrackUtilsTest extends TestCaseWithJsonData{
    
    @Test
    public void readTestTrack() throws IOException{
        TrackData track = JsonLoader.loadMessageFromJson(TrackData.class, getJsonPath("testTrackKeimola.json"));
        // TODO test the Track Utils based on this test track
    }

}
