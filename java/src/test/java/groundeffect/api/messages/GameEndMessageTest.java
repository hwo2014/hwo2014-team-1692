package groundeffect.api.messages;

import groundeffect.api.tools.JsonLoader;

import java.io.IOException;

import org.junit.Test;

public class GameEndMessageTest extends TestCaseWithJsonData {

    @Test
    public void testJson() throws IOException {
        JsonLoader.loadMessageFromJson(GameEndMessage.class, getJsonPath());
    }
}
