package groundeffect.api.messages;

import groundeffect.api.tools.JsonLoader;

import java.nio.file.Path;

public class TestCaseWithJsonData {
    protected Path getJsonPath() {
        return JsonLoader.absolutePathFromClass(getClass(), getClass().getSimpleName() + ".json");
    }
    
    protected Path getJsonPath(String jsonFileName) {
        return JsonLoader.absolutePathFromClass(getClass(), jsonFileName);
    }
}
