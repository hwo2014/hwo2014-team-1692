package groundeffect.api.messages;

import groundeffect.api.tools.JsonLoader;

import java.io.IOException;

import org.junit.Test;

public class GameInitMessageTest extends TestCaseWithJsonData{

    @Test
    public void testJsonPrase() throws IOException {
        JsonLoader.loadMessageFromJson(GameInitMessage.class, getJsonPath());
    }
}
