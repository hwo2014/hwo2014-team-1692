package groundeffect.api.messages;

import groundeffect.api.tools.JsonLoader;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Test;

public class CarPositionsMessageTest extends TestCaseWithJsonData{

    @Test
    public void testJsonPrase() throws IOException {
        CarPositionsMessage msg = JsonLoader.loadMessageFromJson(CarPositionsMessage.class, getJsonPath());
        System.out.println(msg);
    }
}
