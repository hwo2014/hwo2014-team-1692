package groundeffect.api.messages;

import groundeffect.api.tools.JsonLoader;

import java.io.IOException;

import org.junit.Test;

public class LapFinishedMessageTest extends TestCaseWithJsonData{

    @Test
    public void testJson() throws IOException {
        JsonLoader.loadMessageFromJson(LapFinishedMessage.class, getJsonPath());
    }
}
