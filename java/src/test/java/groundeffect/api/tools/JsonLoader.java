package groundeffect.api.tools;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.google.gson.Gson;

public abstract class JsonLoader {
    
    public static <T> Path absolutePathFromClass(Class<T> clazz, String fileName){
        URL fileUrl = clazz.getResource(fileName);
        String filename = fileUrl.getPath();
        Path filePath = Paths.get(filename);
        return filePath;
    }
    
    public static <T> T loadMessageFromJson(Class<T> clazz, Path jsonPath) throws IOException{
        final Gson gson = new Gson();
        byte[] json = Files.readAllBytes(jsonPath);
        return gson.fromJson(new String(json), clazz);
    }
}
