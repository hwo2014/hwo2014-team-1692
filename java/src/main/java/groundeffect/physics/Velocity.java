package groundeffect.physics;

public class Velocity {
    
    public static double getMaximumVelocity(double radius, double friction){
        return Math.sqrt(friction * radius);
    }
    
    public static double getFriction(double velocity, double radius){
        return velocity * velocity * radius;
    }
}
