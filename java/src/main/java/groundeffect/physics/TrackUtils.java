package groundeffect.physics;

import groundeffect.api.model.CarPiecePositionData;
import groundeffect.api.model.TrackData;
import groundeffect.api.model.TrackPieceData;

import java.util.Arrays;

public class TrackUtils {
    
    public static double getTrackLength(TrackData trackData, int laneIndex){
        double distFromCenter = trackData.getLanes()[laneIndex].getDistanceFromCenter();
        return getTrackLength(trackData, distFromCenter);
    }
    
    public static double getTrackLength(TrackData trackData, double distanceFromCenter){
        return Arrays.stream(trackData.getPieces()).mapToDouble(p -> TrackUtils.getPieceLength(p, distanceFromCenter)).sum();
    }
    
    
    private static double getPieceLength(TrackPieceData piece, double distanceFromCenter){
        if (piece.getLength() != 0)
            return piece.getLength();
        else{
            double relAngle = piece.getAngle() / 360.;
            double effectiveRadius = (piece.getRadius() * Math.abs(relAngle) - distanceFromCenter * relAngle);
            return 2. * Math.PI * effectiveRadius;
        }
    }
    
    public static double getDistance(TrackData trackData, CarPiecePositionData oldPosition, CarPiecePositionData newPosition){
        double dist = 0;
        dist += TrackUtils.getPieceLength(trackData.getPieces()[oldPosition.getPieceIndex()], oldPosition.getLane().getStartLaneIndex()) - oldPosition.getInPieceDistance();
        for (int i = oldPosition.getPieceIndex() + 1; i < newPosition.getPieceIndex(); i++){
            dist+=getPieceLength(trackData.getPieces()[i], oldPosition.getLane().getStartLaneIndex());
        }
        dist += newPosition.getInPieceDistance();
        return dist;
    }
}
