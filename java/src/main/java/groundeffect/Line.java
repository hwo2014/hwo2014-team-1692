package groundeffect;

import groundeffect.api.MessageType;

public class Line {

    private MessageType type;
    private String json;

    public Line(MessageType type, String json) {
        this.type = type;
        this.json = json;
    }
    
    public MessageType getType() {
        return type;
    }
    
    public String getJson() {
        return json;
    }
}
