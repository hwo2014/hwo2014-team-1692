package groundeffect;

import groundeffect.api.JsonParser;
import groundeffect.api.MessageType;
import groundeffect.api.commands.AbstractCommand;
import groundeffect.api.commands.JoinCommand;
import groundeffect.api.commands.PingCommand;
import groundeffect.api.commands.ThrottleCommand;
import groundeffect.api.messages.CarPositionsMessage;
import groundeffect.api.messages.DefaultMessage;
import groundeffect.api.messages.GameInitMessage;
import groundeffect.api.model.TrackData;
import groundeffect.physics.TrackUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

public class Main {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        logger.info("Connecting to {}:{} as {} ({})", host, port, botName, botKey);

        try (Socket socket = new Socket(host, port)) {
            final PrintWriter writer = new PrintWriter(new OutputStreamWriter(
                    socket.getOutputStream(), "utf-8"));

            final BufferedReader reader = new BufferedReader(new InputStreamReader(
                    socket.getInputStream(), "utf-8"));

            new Main(reader, writer, botName, botKey);
        }
    }

    final Gson gson = new Gson();
    private PrintWriter writer;
    private String botName;
    private String botKey;
    private BufferedReader reader;
    
    public Main(final BufferedReader reader, final PrintWriter writer, final String botName,
            final String botKey) throws IOException {
        this.reader = reader;
        this.writer = writer;
        this.botName = botName;
        this.botKey = botKey;

        join();

        Line line = read();
        while (line != null) {
            switch (line.getType()) {
                case JOIN:
                    break;
                case GAME_INIT:
                    GameInitMessage gameInitMsg = JsonParser.getMessage(line.getJson(), line.getType(),
                            GameInitMessage.class);
                    
                    TrackData trackData = gameInitMsg.getData().getRace().getTrack();
                     logger.trace(String.valueOf(TrackUtils.getTrackLength(trackData, 0)));
                     logger.trace(String.valueOf(TrackUtils.getTrackLength(trackData, 1)));
                    break;
                case GAME_START:
                case GAME_END:
                    break;

                case CAR_POSITIONS:
                    CarPositionsMessage msg = JsonParser.getMessage(line.getJson(), line.getType(),
                            CarPositionsMessage.class);
                    
                    double angle = msg.getData()[0].getAngle();
                    if (angle < 10 && angle > -10) {
                        throttle(0.7);
                    } else {
                        throttle(0.3);
                    }
                    
                    break;

                case YOUR_CAR:
                case CRASH:
                case SPAWN:
                case FINISH:
                case LAP_FINISHED:
                case TOURNAMENT_END:
                case OTHER:
                    ping();
                    break;

            }
            line = read();
        }
    }

    private Line read() throws IOException {
        String line = reader.readLine();
        if (line == null) {
            return null;
        }
        

        logger.trace("Received line: {}", line);

        DefaultMessage message = gson.fromJson(line, DefaultMessage.class);
        return new Line(MessageType.valueOfJsonType(message.getMsgType()), line);
    }

    private void join() {
        send(new JoinCommand(botName, botKey));
    }

    private void ping() {
        send(new PingCommand());
    }

    private void throttle(double throttle) {
        send(new ThrottleCommand(throttle));
    }

    private void send(AbstractCommand command) {
        String cmdString = command.toJson();
        logger.trace("Sending command: {}", cmdString);
        writer.println(cmdString);
        writer.flush();
    }
}