package groundeffect.api.commands;

public class JoinCommand extends AbstractCommand {
    private String name;
    private String key;

    public JoinCommand(String botName, String botKey) {
        super("join");
        this.name = botName;
        this.key = botKey;
    }

    public String getName() {
        return name;
    }
    
    public String getKey() {
        return key;
    }
    
    public Object getData() {
        return this;
    }
}
