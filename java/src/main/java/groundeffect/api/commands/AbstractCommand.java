package groundeffect.api.commands;

public abstract class AbstractCommand {

    private transient String messageType;

    public AbstractCommand(String messageType) {
        this.messageType = messageType;
    }
    
    public abstract Object getData();
    
    public String toJson() {
        return new CommandWrapper(this.messageType, this).toJson();
    }
}
