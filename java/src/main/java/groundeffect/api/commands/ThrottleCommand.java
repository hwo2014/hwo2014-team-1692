package groundeffect.api.commands;

public class ThrottleCommand extends AbstractCommand {

    private double throttle;

    public ThrottleCommand(double throttle) {
        super("throttle");
        this.throttle = throttle;
    }
    
    @Override
    public Object getData() {
        return this.throttle;
    }
    
    public double getThrottle() {
        return throttle;
    }
}
