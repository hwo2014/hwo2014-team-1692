package groundeffect.api.commands;

public class SwitchLaneCommand extends AbstractCommand {
    public static enum Direction {
        Left, Right
    }

    private Direction direction;

    public SwitchLaneCommand(Direction direction) {
        super("switchLane");
        this.direction = direction;
    }
    
    @Override
    public Object getData() {
        return direction;
    }
    
    public Direction getDirection() {
        return direction;
    }
}
