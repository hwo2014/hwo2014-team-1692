package groundeffect.api.commands;

import com.google.gson.Gson;


public class CommandWrapper {
    private static final Gson gson = new Gson();
    
    private String msgType;
    private Object data;

    CommandWrapper(String msgType, AbstractCommand command) {
        this.msgType = msgType;
        this.data = command.getData();
    }
    
    public String getMsgType() {
        return msgType;
    }
    
    public Object getData() {
        return data;
    }
    
    public String toJson() {
        return gson.toJson(this);
    }
}
