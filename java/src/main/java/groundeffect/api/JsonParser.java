package groundeffect.api;

import groundeffect.api.messages.DefaultMessage;

import com.google.gson.Gson;

public class JsonParser {
    private static Gson gson = new Gson();

    public static MessageType getMessageType(String json) {
        String typeName = gson.fromJson(json, DefaultMessage.class).getMsgType();
        return MessageType.valueOfJsonType(typeName);
    }

    public static <T extends DefaultMessage> T getMessage(String json, MessageType type,
            Class<T> clazz) {
        T instance = clazz.cast(gson.fromJson(json, type.getClazz()));
        return instance;
    }
}
