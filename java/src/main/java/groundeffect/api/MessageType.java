package groundeffect.api;

import groundeffect.api.messages.CarPositionsMessage;
import groundeffect.api.messages.CrashMessage;
import groundeffect.api.messages.DefaultMessage;
import groundeffect.api.messages.FinishMessage;
import groundeffect.api.messages.GameEndMessage;
import groundeffect.api.messages.GameInitMessage;
import groundeffect.api.messages.GameStartMessage;
import groundeffect.api.messages.JoinMessage;
import groundeffect.api.messages.LapFinishedMessage;
import groundeffect.api.messages.SpawnMessage;
import groundeffect.api.messages.TournamentEndMessage;
import groundeffect.api.messages.YourCarMessage;

public enum MessageType {
    JOIN("join", JoinMessage.class), //$NON-NLS-1$
    YOUR_CAR("yourCar", YourCarMessage.class), //$NON-NLS-1$
    GAME_INIT("gameInit", GameInitMessage.class), //$NON-NLS-1$
    GAME_START("gameStart", GameStartMessage.class), //$NON-NLS-1$
    CAR_POSITIONS("carPositions", CarPositionsMessage.class), //$NON-NLS-1$
    CRASH("crash", CrashMessage.class), //$NON-NLS-1$
    SPAWN("spawn", SpawnMessage.class), //$NON-NLS-1$
    LAP_FINISHED("lapFinished", LapFinishedMessage.class), //$NON-NLS-1$
    FINISH("finish", FinishMessage.class), //$NON-NLS-1$
    GAME_END("gameEnd", GameEndMessage.class), //$NON-NLS-1$
    TOURNAMENT_END("tournamentEnd", TournamentEndMessage.class), //$NON-NLS-1$
    OTHER("", DefaultMessage.class); //$NON-NLS-1$

    private String jsonName;
    private Class<? extends DefaultMessage> clazz;

    MessageType(String jsonName, Class<? extends DefaultMessage> clazz) {
        this.jsonName = jsonName;
        this.clazz = clazz;
    }

    public String getJsonName() {
        return jsonName;
    }

    public Class<? extends DefaultMessage> getClazz() {
        return clazz;
    }

    public static MessageType valueOfJsonType(String typeName) {
        for (MessageType type : values()) {
            if (type.getJsonName().equals(typeName)) {
                return type;
            }
        }
        return OTHER;
    }
}
