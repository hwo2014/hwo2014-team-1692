package groundeffect.api.model;

public class CarData {
    private CarIdData id;
    private CarDimensionData dimensions;

    public CarIdData getId() {
        return id;
    }

    public CarDimensionData getDimensions() {
        return dimensions;
    }
}
