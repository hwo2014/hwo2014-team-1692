package groundeffect.api.model;

public class GameEndData {
    private GameResultData[] results;
    private GameResultLapData[] bestLaps;

    public GameResultData[] getResults() {
        return results;
    }

    public GameResultLapData[] getBestLaps() {
        return bestLaps;
    }
}
