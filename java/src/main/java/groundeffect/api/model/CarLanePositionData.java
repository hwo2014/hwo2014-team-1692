package groundeffect.api.model;

public class CarLanePositionData {
    private double startLaneIndex;
    private double endLaneIndex;

    public int getStartLaneIndex() {
        return (int) startLaneIndex;
    }

    public int getEndLaneIndex() {
        return (int) endLaneIndex;
    }
}
