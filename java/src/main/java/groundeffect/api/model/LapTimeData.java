package groundeffect.api.model;

public class LapTimeData {
    private double lap;
    private double ticks;
    private double millis;

    public int getLap() {
        return (int) lap;
    }

    public int getTicks() {
        return (int) ticks;
    }

    public int getMillis() {
        return (int) millis;
    }
}
