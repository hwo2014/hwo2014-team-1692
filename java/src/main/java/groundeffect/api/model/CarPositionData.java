package groundeffect.api.model;

public class CarPositionData {
    private CarIdData id;
    private double angle;
    private CarPiecePositionData piecePosition;

    public Object getId() {
        return id;
    }

    public double getAngle() {
        return angle;
    }

    public CarPiecePositionData getPiecePosition() {
        return piecePosition;
    }
}