package groundeffect.api.model;

public class RaceSessionData {
    private double laps;
    private double maxLapTimeMs;
    private boolean quickRace;

    public int getLaps() {
        return (int) laps;
    }

    public int getMaxLapTimeMs() {
        return (int) maxLapTimeMs;
    }

    public boolean isQuickRace() {
        return quickRace;
    }
}
