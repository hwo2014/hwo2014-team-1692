package groundeffect.api.model;

public class LapData {
    private CarIdData car;
    private LapTimeData lapTime;
    private RaceTimeData raceTime;
    private RankingData ranking;

    public CarIdData getCar() {
        return car;
    }

    public LapTimeData getLapTime() {
        return lapTime;
    }

    public RaceTimeData getRaceTime() {
        return raceTime;
    }

    public RankingData getRanking() {
        return ranking;
    }
}
