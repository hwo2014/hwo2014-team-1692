package groundeffect.api.model;

public class JoinData {
    private String name;
    private String key;

    public String getName() {
        return name;
    }

    public String getKey() {
        return key;
    }
}