package groundeffect.api.model;

public class CarPiecePositionData {
    private double pieceIndex;
    private double inPieceDistance;
    private CarLanePositionData lane;
    private double lap;

    public int getPieceIndex() {
        return (int) pieceIndex;
    }

    public double getInPieceDistance() {
        return inPieceDistance;
    }

    public CarLanePositionData getLane() {
        return lane;
    }

    public int getLap() {
        return (int) lap;
    }
}
