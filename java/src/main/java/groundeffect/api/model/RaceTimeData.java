package groundeffect.api.model;

public class RaceTimeData {
    private double laps;
    private double ticks;
    private double millis;

    public int getLaps() {
        return (int) laps;
    }

    public int getTicks() {
        return (int) ticks;
    }

    public int getMilis() {
        return (int) millis;
    }
}
