package groundeffect.api.model;

import com.google.gson.annotations.SerializedName;

public class TrackPieceData {
    @SerializedName("swich")
    private boolean isSwitch = false;
    private double length = 0;
    private double radius = 0;
    private double angle = 0;

    public boolean isSwitch() {
        return isSwitch;
    }

    public boolean isBend() {
        return radius > 0 && angle > 0;
    }

    public double getLength() {
        return length;
    }

    public double getRadius() {
        return radius;
    }

    public double getAngle() {
        return angle;
    }
}
