package groundeffect.api.model;

public class TrackLaneData {
    private double index;
    private double distanceFromCenter;

    public int getIndex() {
        return (int) index;
    }

    public double getDistanceFromCenter() {
        return distanceFromCenter;
    }
}
