package groundeffect.api.model;

public class GameResultLapData {
    private CarIdData car;
    private LapTimeData result;

    public CarIdData getCar() {
        return car;
    }

    public LapTimeData getResult() {
        return result;
    }
}
