package groundeffect.api.model;

public class TrackPointData {
    public static class TrackPositionData {
        private double x;
        private double y;

        public double getX() {
            return x;
        }

        public double getY() {
            return y;
        }
    }

    private TrackPositionData position;
    private double angle;

    public TrackPositionData getPosition() {
        return position;
    }

    public double getAngle() {
        return angle;
    }
}
