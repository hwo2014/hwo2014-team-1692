package groundeffect.api.model;

public class RaceData {
    private TrackData track;
    private CarData[] cars;
    private RaceSessionData raceSession;

    public TrackData getTrack() {
        return track;
    }

    public CarData[] getCars() {
        return cars;
    }

    public RaceSessionData getRaceSession() {
        return raceSession;
    }
}