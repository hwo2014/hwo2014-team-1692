package groundeffect.api.model;

public class GameResultData {
    private CarIdData car;
    private RaceTimeData result;

    public CarIdData getCar() {
        return car;
    }

    public RaceTimeData getResult() {
        return result;
    }
}
