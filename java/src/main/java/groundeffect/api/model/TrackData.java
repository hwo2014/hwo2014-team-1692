package groundeffect.api.model;

public class TrackData {
    private String id;
    private String name;
    private TrackPieceData[] pieces;
    private TrackLaneData[] lanes;
    private TrackPointData startingPoint;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public TrackPieceData[] getPieces() {
        return pieces;
    }

    public TrackLaneData[] getLanes() {
        return lanes;
    }

    public TrackPointData getStartingPoint() {
        return startingPoint;
    }
}