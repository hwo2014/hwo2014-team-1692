package groundeffect.api.model;

public class RankingData {
    private double overall;
    private double fastestLap;

    public int getOverall() {
        return (int) overall;
    }

    public int getFastestLap() {
        return (int) fastestLap;
    }
}
