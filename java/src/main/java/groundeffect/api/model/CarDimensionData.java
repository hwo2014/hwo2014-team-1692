package groundeffect.api.model;

public class CarDimensionData {
    private double length;
    private double width;
    private double guideFlagPosition;

    public double getLength() {
        return length;
    }

    public double getWidth() {
        return width;
    }

    public double getGuideFlagPosition() {
        return guideFlagPosition;
    }
}
