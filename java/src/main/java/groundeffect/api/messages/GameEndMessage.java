package groundeffect.api.messages;

import groundeffect.api.model.GameEndData;

public class GameEndMessage extends DefaultMessage {
    private GameEndData data;

    public GameEndData getData() {
        return data;
    }
}
