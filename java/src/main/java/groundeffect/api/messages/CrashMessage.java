package groundeffect.api.messages;

import groundeffect.api.model.CarIdData;

public class CrashMessage extends DefaultMessage {

    private CarIdData data;

    public CarIdData getData() {
        return data;
    }
}
