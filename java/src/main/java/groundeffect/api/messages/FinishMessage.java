package groundeffect.api.messages;

import groundeffect.api.model.CarIdData;

public class FinishMessage extends DefaultMessage {
    private CarIdData data;

    public CarIdData getData() {
        return data;
    }
}
