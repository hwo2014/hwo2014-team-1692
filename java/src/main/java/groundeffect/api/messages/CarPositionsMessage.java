package groundeffect.api.messages;

import groundeffect.api.model.CarPositionData;

public class CarPositionsMessage extends DefaultMessage {

    private CarPositionData[] data;

    public CarPositionData[] getData() {
        return data;
    }
}
