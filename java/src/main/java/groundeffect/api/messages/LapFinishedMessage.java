package groundeffect.api.messages;

import groundeffect.api.model.LapData;

public class LapFinishedMessage extends DefaultMessage {
    private LapData data;

    public LapData getData() {
        return data;
    }
}
