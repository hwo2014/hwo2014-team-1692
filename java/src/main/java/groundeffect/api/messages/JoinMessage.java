package groundeffect.api.messages;

import groundeffect.api.model.JoinData;

public class JoinMessage extends DefaultMessage {
    private JoinData data;

    public JoinData getData() {
        return data;
    }
}
