package groundeffect.api.messages;

import groundeffect.api.model.GameData;

public class GameInitMessage extends DefaultMessage {
    private GameData data;

    public GameData getData() {
        return data;
    }
}
